#!/bin/sh
time_stamp=$(date +%Y-%m-%d-%T)
ids_dirty="ids_dirty.txt"
ids_clean="ids.txt"
echo "Making new directory for today"
mkdir -p "archives/${time_stamp}"
cd "archives/${time_stamp}"
echo "Downloading pages with wget"
wget --user-agent="Googlebot" --convert-links https://voat.co/v/pizzagate/new?page={1..19}
echo "Now finding unique ids"
grep -o -h "/v/pizzagate/[0-9][0-9][0-9][0-9][0-9][0-9][0-9]" *page* > "$ids_dirty"
echo "Cleaning up data"
sed -i 's/[^0-9]*//g' "$ids_dirty" 
echo "Sorting unique ids"
sort -u "$ids_dirty" > "$ids_clean"
echo "Removing dirty ids file"
rm "$ids_dirty"
echo "downloading all posts"
mkdir -p "posts"
wget --user-agent="Googlebot" --convert-links --directory-prefix="posts" --base="https://voat.co/v/pizzagate/" --input-file="${ids_clean}"
echo "finding all external links"
grep -Eo -h "(http|https)://[a-zA-Z0-9./?=_-]*" posts/* > external.txt
echo "cleaning and sorting external links"
egrep -v "voat.co|archive.is" external.txt > external-dirty.txt
sort -u external-dirty.txt > external-links.txt
rm external-dirty.txt external.txt
echo "zipping posts to save space"
zip -r -mT posts/posts.zip posts/*
#echo "backing up voat posts to archive.is"
#while read p; do
#  curl --data "url=https://voat.co/v/pizzagate/${p}" http://archive.is/submit/
#done <"$ids_clean"
#echo "backing up external links in voat posts to archive.is"
#while read p; do
#  curl --data "url=${p}" http://archive.is/submit/
#done <external-links.txt
echo "time to commit"
git add -A
git commit -m "${time_stamp}"
git push
echo "Done. Save the children!"
